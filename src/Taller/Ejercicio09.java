package Taller;
import java.util.Scanner;

public class Ejercicio09 {
	
	static Scanner leer = new Scanner (System.in);
	
	public void Salary(double hourPrice, double hour, double salary, double result) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Salario Trabajador ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese las horas trabajadas del trabajador: ");
		hour = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el precio de la hora: ");
		hourPrice  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		salary = (hour*hourPrice);
		result = (salary-(salary*0.2));
		System.out.println("El salario neto del trabajador con un 20% de descuento por impuesto es: " + result);
		
	}

}
