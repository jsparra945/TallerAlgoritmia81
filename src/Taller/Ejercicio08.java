package Taller;
import java.util.Scanner;

public class Ejercicio08 {
	
	static Scanner leer = new Scanner (System.in); 
	
	public void areaTriangle(double side1, double side2, double side3, double area, double perimeter , double aux) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" area de un triangulo ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la longitud del primer lado: ");
		side1  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la longitud del segundo lado: ");
		side2  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la longitud del tercer lado: ");
		side3  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		perimeter = (side1 + side2 + side3)/2;
		aux = perimeter*(perimeter-side1)*(perimeter-side2)*(perimeter-side3);
		area = Math.sqrt(aux);
		System.out.println("El area del triangulo es: " + area);
		
	}

}
