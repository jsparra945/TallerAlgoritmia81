package Taller;
import java.util.Scanner;

public class Ejercicio06 {
	
	static Scanner leer = new Scanner(System.in);
	
	public void Percentage(double numbersMan, double numbersWoman, double percentageMan, double percentageWoman, double sum) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Porcentaje de hombres y Mujeres ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la cantidad de hombres: ");
		numbersMan  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la cantidad de mujeres: ");
		numbersWoman  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		sum = numbersMan+numbersWoman;
		percentageMan = (numbersMan/sum)*100;
		percentageWoman = (numbersWoman/sum)*100;
		System.out.println("El porcentaje de hombres es de: " + percentageMan + " %\n");
		System.out.println("Y el porcentaje de mujeres es de: " + percentageWoman + " %\n");
		
	}

}
