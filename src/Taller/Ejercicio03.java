package Taller;
import java.util.Scanner;

public class Ejercicio03 {

	static Scanner leer = new Scanner(System.in);
	
	public void Seller(double sale1, double sale2, double sale3, double commission, double salary, double result) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Sueldo base y sus comisiones "); 
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el sueldo base: ");
		salary  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Comisiones "); 
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el valor de la primera venta: ");
		sale1  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el valor de la segunda venta: ");
		sale2  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el valor de la tercera venta: ");
		sale3  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		commission = (sale1 + sale2 + sale3)*0.1;
		System.out.print("Dinero que obtendra por comisiones de venta: " + commission);
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		result = commission + salary;
		System.out.print("El total que recibira por sueldo base y comisiones es de: " + result);
		
	}

}
