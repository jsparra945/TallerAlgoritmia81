package Taller;
import java.util.Scanner;

public class Ejercicio05 {
	
	static Scanner leer = new Scanner (System.in);
	
	public void Qualification(double Q1, double Q2, double Q3, double average, double test, double work, double note) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Nota final de computo ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la calificacion parcial 1: ");
		Q1  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la calificacion parcial 2: ");
		Q2  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la calificacion parcial 3: ");
		Q3  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		average = (Q1+Q2+Q3)/3; 
		System.out.println("El promedio de las calificaciones parciales: " + average);
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la calificacion del examen final: ");
		test  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la calificacion del trabajo final: ");
		work  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		note = (average*0.55)+(work*0.3)+(test*0.15);
		System.out.println("La calificacion parcial es de: " + note);
		
	}

}
