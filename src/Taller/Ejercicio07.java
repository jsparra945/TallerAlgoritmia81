package Taller;
import java.util.Scanner;

public class Ejercicio07 {
	
	Scanner leer = new Scanner(System.in);
	
	public void Conversion(double meter, double inches, double feet) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Conversion a pies y pulgadas ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la cantidad de metros: ");
		meter = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		inches = meter*39.27;
		feet = inches/12;
		System.out.println("La conversion a pulgadas es: " + inches + "\n");
		System.out.println("La conversion a pies es: " + feet + "\n");
		
	}

}
