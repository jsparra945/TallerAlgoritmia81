package Taller;
import java.util.Scanner;

public class Ejercicio02 {
	
	static Scanner leer = new Scanner(System.in);
	
	public void Capital(double capital, double gain) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Ganancia capital despues de un mes ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el capital a invertir: ");
		capital = leer.nextInt();
		gain = (capital*0.02);
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Despues de un mes ganara : " + gain);
		capital = leer.nextInt();
		
	}

}
