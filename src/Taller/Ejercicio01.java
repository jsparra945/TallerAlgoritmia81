package Taller;
import java.util.Scanner;

public class Ejercicio01 {
	
	static Scanner leer = new Scanner(System.in);

	public void Average(double agePerson1, double agePerson2, double agePerson3, double result ) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Promedio de edad de tres personas ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la edad de la primera persona: ");
		agePerson1  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la edad de la segunda persona: ");
		agePerson2  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese la edad de la tercera persona: ");
		agePerson3  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		result = (agePerson1 + agePerson2 + agePerson3)/3;
		System.out.println("El promedio de edad de las tres personas es: " + result);
		
	}
	
}
