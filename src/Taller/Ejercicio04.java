package Taller;
import java.util.Scanner;

public class Ejercicio04 {
	
	static Scanner leer = new Scanner (System.in);
	
	public void Store ( double purchase, double result) {
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.println(" Descuento del 15% ");   
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		System.out.print("Ingrese el valor de la compra: ");
		purchase  = leer.nextInt();
		System.out.println("\n- - - - - - - - - - - - - - - - - - -\n");
		result = (purchase-(purchase*0.15));
		System.out.println("El total de la compra con el 15% de descuento es de: " + result);
		
	}

}
